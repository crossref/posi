---
title: Community Discussion
subtitle: A history of the discussion within all parts of the open scholarly infrastructure community.
date: "2022-11-16"
publishdate: "2024-11-30"
comments: false
---

For decades, science and research infrastructure initiatives have been working together to form norms and practices to ensure long-term sustainability of critical systems on which researchers rely. 

The following key documents and talks (listed chronologically) discuss or draw on the POSI principles. Please [make a merge request via GitLab](https://gitlab.com/crossref/posi/-/merge_requests/new) to list any additional relevant resources.

| Year & month	| Title	| Authors |
|--------------------------	|---	|---|
| 2015-February	| (Originally-proposed) [Principles for Open Scholarly Infrastructures](http://dx.doi.org/10.6084/m9.figshare.1314859)	| Neylon, Bilder, Lin |
| 2015-August	| [What exactly is infrastructure? Seeing the leopard's spots](http://dx.doi.org/10.6084/m9.figshare.1520432) 	| Neylon, Bilder, Lin |
| 2016-January	| [Where are the pipes? Building Foundational Infrastructures for Future Services](http://cameronneylon.net/blog/where-are-the-pipes-building-foundational-infrastructures-for-future-services/) 	| Neylon, Bilder, Lin |
| 2016-April	| [A Healthy Research Ecosystem: Diversity by Design](https://doi.org/10.15200/winn.146047.79215) 	| Chodacki, Cruse, Lin, Neylon |
| 2016-July	| [Squaring Circles: The economics and governance of scholarly infrastructures](http://cameronneylon.net/blog/squaring-circles-the-economics-and-governance-of-scholarly-infrastructures/)	| Neylon |
| 2018-April	| [Supporting Research Communications: A Guide](https://doi.org/10.5281/zenodo.3524663)	| Chodacki et al |
| 2019-January	| [Good Practice Principles for Scholarly Communication Services](https://sparcopen.org/our-work/good-practice-principles-for-scholarly-communication-services/) 	| COAR-SPARC |
| 2019-April	| [EXAMPLARITY CRITERIA for funding from the National Open Science Fund through platforms, infrastructures and editorial content](https://www.ouvrirlascience.fr/examplarity-criteria-for-funding-from-the-national-open-science-fund/) 	| French National Open Science Fund |
| 2020-October	| [Living Our Values and Principles: Exploring Assessment Strategies for the Scholarly Communication Field](https://educopia.org/living-our-values-and-principles/) 	| Skinner and Wipperman, Educopia Institute |
| 2021-January	| [Why openness makes research infrastructure resilient](https://doi.org/10.1002/leap.1361) 	| Cousijn, Hendricks, Meadows |
| 2021-October	| [Now is the time to work together toward open infrastructures for scholarly metadata](https://blogs.lse.ac.uk/impactofsocialsciences/2021/10/27/now-is-the-time-to-work-together-toward-open-infrastructures-for-scholarly-metadata/) 	| Hendricks et al |
| 2021-November	| [Beyond open: Key criteria to assess open infrastructure](https://investinopen.org/blog/criteria-to-assess-openinfra/) 	| Invest In Open |
| 2022-February	| [Assessing data infrastructure: the Principles of Open Scholarly Infrastructure](https://blog.ldodds.com/2022/02/23/assessing-data-infrastructure-the-principles-of-open-scholarly-infrastructure/) 	| Dodds |
| 2023-July | [Rules vs. Principles in POSI](https://eve.gd/2023/07/28/rules-vs-principles-in-posi) | Eve |
| 2023-November | [Launching (POSI) version 1.1: Reflections from adopters](https://doi.org/10.54900/n6az7-4xb07) | Hendricks et al |
| 2024-August | [The Principles of Fauxpen Scholarly Infrastructure](https://www.youtube.com/watch?v=DZ2Bgwyx3nU) | Bilder |
| 2024-October | [ORCID’s position on POSI](https://info.orcid.org/orcids-self-assessment-of-the-posi-principles/) | Shillum |

---

All POSI adopters welcome questions and suggestions about The Principles of Open Scholarly Infrastructure. If your question is not answered in the [About & FAQ page](https://openscholarlyinfrastructure.org/faq/), we encourage you to ask your questions in public using our issue tracker because that allows others who are interested in POSI to comment and contribute to the conversation as well.

> [Submit your question and/or suggestion openly here](https://gitlab.com/crossref/posi/-/issues/new)

You can also open an issue via email by clicking on the "Email a new issue to this project" link.

If you would like to contact someone privately, please email anyone from the [POSI adopters group](/posse/), since there is no one organisation behind POSI. If you don't know anyone, try emailing one of:

- Sarah Lippincott: sarah [at] datadryad [dot] org (Dryad)
- Matthew Buys: mattbuys [at] datacite [dot] org (DataCite)
- Lucy Ofiesh: lofiesh [at] crossref [dot] org (Crossref)
- John Chodacki: john [at] ROR [dot] org (ROR)

... and somebody from one of the POSI adopters will get in touch with you directly.
