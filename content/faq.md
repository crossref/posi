---
title: About POSI and FAQ
subtitle: Frequently-answered questions about POSI
date: "2020-11-26"
publishdate: "2020-11-20"
comments: false
---

## Why POSI? Background and motivation

First developed in a [2015 blog post](https://cameronneylon.net/blog/principles-for-open-scholarly-infrastructures/), POSI offers a set of guidelines by which open scholarly infrastructure organisations and initiatives that support the research community can be run and sustained. This site, launched in 2020, codifies POSI as a community resource. Many open infrastructure organisations are at different stages of their journeys toward compliance with POSI. As more attention is paid to open infrastructure and the [community discusses where POSI fits in](/discussion), wwe needed a place where POSI can be more closely integrated into the community's vision for open research.

## Who adopts POSI?

Any organisation that provides open infrastructure for research and scholarly communications is welcome to do their own self-assessment against the Principles. The list of adopters is kept current by the [adopting organisations](/posse) and anyone can make a merge request to add their assessment or a discussion post. Take a look at the [community discussion](/discussion) and, once you've publicly committed, one of the [adopters](/posse) will add you to the informal Slack chat and email list for adopters.

---

## Do you have to meet all the commitments of POSI before you can adopt POSI?

No. For one thing, this would make it impossible for new organisations to adopt POSI. Our experience in the founding of infrastructure services like Dryad, DataCite, ORCID, ROR, and Crossref is that one of the biggest challenges to developing any new infrastructure is gaining the trust of the community. One powerful way to address the community’s legitimate skepticism, is to adopt a set of operating principles from the start. They can then help to shape foundational decisions about governance, funding,  and technology strategies. We would also like to see existing organisations adopt POSI as a statement of intent. Doing so provides their stakeholders with a set of concrete commitments against which the organisation can be measured.

---

## Isn’t adopting POSI “Virtue Signalling?”

Yes. This shouldn’t be surprising or contentious. Our entire scholarly communication system is based on virtue signalling. But, of course, the term “virtue signalling” (with scare-quotes) is also sometimes used to insinuate that such signalling is disingenuous and designed primarily for marketing purposes.

The principles were drafted with a built-in safeguard against disingenuous use.

Adoption of the principles is verifiable. You either have or don’t have broad stakeholder governance. You either have or don’t have a surplus. You either have or don’t have contingency funds. The source code for your infrastructure is either open source or it is not.

This means that it is possible for a party to adopt the principles as an aspirational statement, even when they do not yet meet all (or even any) of the requirements. The community can then monitor progress on the individual actions and detect when no progress is being made. The reputational fallout from publicly adopting the principles and then failing to make any progress on them would be severe. Public adoption of the principles as an aspiration becomes a kind of [forcing function](https://en.wikipedia.org/wiki/Behavior-shaping_constraint).

---

## What is meant by “stakeholder?” Does this just mean those who have made direct financial investments in the infrastructure organisation?

No. This narrow definition of “stakeholder” - focusing solely on those who have “invested” -  is not widely held. In fact, common phrases like "[stakeholder economy](https://www.lexico.com/definition/stakeholder_economy)" and "[stakeholder capitalism](https://hbr.org/2020/01/making-stakeholder-capitalism-a-reality)" describe the exact opposite: systems that don't just focus on the “investor”, but which instead balance benefits to the investor with benefits to employees, the broader community, society, and the environment.

It is this latter, broader definition of “stakeholder” that is used in POSI. Stakeholders referenced in POSI reflect those that have a role in the global scholarly enterprise.

---

## Does POSI exclude commercial organisations that provide open scholarly infrastructure?

No. In fact, one of the motivations in developing POSI was to set out some guidelines that could be incorporated into the procurement rules for assessing candidates for public/private partnerships. POSI offers clear ways to compare organisational governance, sustainability and insurance practices irrespective of the organisation’s tax status. These can also be used to evaluate the practices of community-run projects that may not be a legal entity or have formal governance documents.

We also think that wide adoption of these principles will actually help commercial organisations because it will set some clear ground-rules about how they can invest in creating services for the community in a way that respects the community’s interests and gains the community’s trust. This, in turn, minimises the financial risk of investing heavily in something that the community ultimately rejects due to concerns about enclosure, privacy, fee hikes, or other evils that are (sometimes unfairly) associated with a service just because it’s not run by a nonprofit. Equally, being a nonprofit does not make adherence to POSI automatic.

---

## There are points made in POSI about open and available data. Why not refer to the FAIR principles?

First, POSI was developed before FAIR. But, more importantly, although FAIR is a relevant and useful list of principles, it doesn’t speak to core requirements of infrastructure organisations. Nor, to many people's surprise, does _FAIR_ focus on striving for openness.

---

## Is adoption or endorsement of POSI only relevant to organisations actually running (or planning on running) scholarly infrastructure?

No. The entire scholarly community needs to take responsibility for ensuring that scholarly infrastructure remains open. POSI offers a way to articulate and assess this.  We hope that POSI will be incorporated into institutional procurement processes, funder grant rules, and in community measurements of success.

---

## Why not “open science infrastructure” instead of “open scholarly infrastructure?”

Because the research enterprise increasingly transcends disciplines. And we think this is to be encouraged.

In some parts of the world, science and technology are increasingly seen as being out-of-touch with the ethical, social, and cultural concerns of the communities within which they operate.  We are concerned that infrastructures that focus exclusively on “science” will simply further entrench the “two cultures” divide and exacerbate this trend.

---

## Why not “open research infrastructure” instead of “open scholarly infrastructure?”

Because scholarship involves more than “research.” It also includes “teaching”, for example. And we think POSI is as applicable to teaching infrastructures as to research infrastructures.

---

## Do the POSI principles only apply to membership organisations?

No. The principles mostly use the word “member” in the sense of “member of the community.” Although the principles were inspired by the experiences two membership organisations and the examples mentioned (particularly around sustainability) reflect that, the principles make the point that it is important that we explore other models as well. The overall goals are to ensure that the organisation is responsive to members of the community and that it is not entirely dependent on inherently brittle short-term funding cycles. So, for example, one might explore how to normalise the use of long-term grants or partial endowments as the basis for funding.

---


## How do you pronounce "POSI?"

In the past we've pronounced it a number of ways, but we've settled on "posy"- [as in a small bouquet of flowers](https://en.wiktionary.org/wiki/posy) 💐

---

## Can the rules in POSI be flexible?

Absolutely. Clauses such as the "1 year of reserves" wind-down clause are meant to achieve a goal/principle, not to be treated as absolutes in themselves. If your organisation requires much less than a year to wind down (or requires much more), this should be adjusted. A good rule of thumb is that you should read POSI statements as _principles_ rather than _rules_.

---

## How does POSI stay up to date?

Based on the active and practical experience and interpretation of the then-fifteen adopters, in 2023, the Principles we're updated to clarify language. [This website shows the latest v1.1 version](/) and has also archived the [original](/posi-v1.0) and the [tracked changes between the two versions](/posi-v1.1-revisions.pdf). It's important that updates made to the Principles come from those who are following and enacting them day by day, therefore ensuring any changes are made on real-life experience. The adopters are currently discussing POSI v2 and a public response document will be available in early 2025.

---

## I want to use a more restrictive license, because I am worried about third-parties taking our data and re-selling it

While this is a possibility, we have rarely seen it happen. Furthermore, given that the original data are open and available for free, this is not an easy challenge. Generally speaking, using more restrictive licenses merely ends up frustrating well-intentioned downstream re-users and causes problems if a rescue operation is needed for the infrastructure. An example of problematic licensing might be using CC BY-SA on data that researchers might wish to combine with proprietary datasets. This is why we recommend the public domain declaration, CC0.

---

## How do I cite POSI?
Cite as `Bilder G, Lin J, Neylon C (2020), The Principles of Open Scholarly Infrastructure, retrieved [date], https://doi.org/10.24343/C34W2H `

---

More questions? Open a [GitLab issue](https://gitlab.com/crossref/posi/-/issues).
