---
title: POSI Adopters
subtitle: Which organisations have publicly committed to the Principles?
date: "2024-07-03"
publishdate: "2021-03-09"
comments: false
---
These organizations or initiatives (listed alphabetically) have formally adopted the POSI principles by publishing an initial self-audit, and committed to routinely demonstrating evidence of following POSI in practice.

1. [CLOCKSS](https://clockss.org/the-alignment-of-posi-principles-and-clockss/) (original posted 2024-09-10)
2. [CORE](https://blog.core.ac.uk/2022/05/23/core-our-commitment-to-the-principles-of-open-scholarly-infrastructure/) (original posted 2022-May-23)
3. [COUNTER](https://www.countermetrics.org/counter-posi/) (original posted 2024-05-30)
4. Crossref: [POSI fan tutte](https://www.crossref.org/blog/posi-fan-tutte/) (2022-March-08) and [original](https://www.crossref.org/blog/crossrefs-board-votes-to-adopt-the-principles-of-open-scholarly-infrastructure) (2020-December-02)
5. [DataCite](https://doi.org/10.5438/vy7h-g464) (original posted 2021-August-30)
6. [DOAJ](https://blog.doaj.org/2022/10/06/doaj-commits-to-the-principles-of-open-scholarly-infrastructure-posi/) (original posted 2022-October-06)
7. [Dryad](https://blog.datadryad.org/2020/12/08/dryads-commitment-to-the-principles-of-open-scholarly-infrastructure/) (original posted 2020-December-08)
8. Europe PMC: [Moving to open source](https://blog.europepmc.org/2024/02/moving-to-open-source.html) (2024-February-26) and [original](http://blog.europepmc.org/2022/02/EPMC-adopts-POSI.html) (2022-February-21)
9. [Hal+](https://www.ccsd.cnrs.fr/en/2024/11/hal-plus-is-committed-to-the-posi-principles/) (original posted 2024-November-28)
10. [JOSS](https://blog.joss.theoj.org/2021/02/JOSS-POSI) (original posted 2021-February-14)
11. Liberate Science: [Reevaluating POSI: 2023 evolution](https://libscie.org/reevaluating-posi-2023-evolution) (2023-November-14) and [original](https://libscie.org/principles-of-open-scholarly-infrastructure-2022/) (2022-August-02)
12. [OAPEN and DOAB](https://doi.org/10.58079/sh4g) (original posted  2023-May-04)
13. [OA Switchboard](https://www.oaswitchboard.org/blog7oct2021) (original posted 2021-October-07)
14. [OpenAIRE](https://www.openaire.eu/principles-open-scholarly-infrastructure-openaire-assessment) (original posted 2022-March-29)
15. [OpenCitations](https://opencitations.wordpress.com/2021/08/09/opencitations-compliance-with-the-principles-of-open-scholarly-infrastructure/) (original posted 2021-August-09)
16. [OPERAS](https://operas-eu.org/principles-of-open-scholarly-infrastructure-posi/) (original posted 2024-September-25)
17. [OurResearch](https://blog.ourresearch.org/posi/) (original posted 2021-June-10)
18. [Peer Community In](https://peercommunityin.org/2024/04/11/posi/) (original posted 2024-April-11)
19. [Public Knowledge Project](https://pkp.sfu.ca/2024/05/28/pkp-signs-on-to-the-posi/) (original posted 2024-May-28)
20. [ROR](https://ror.org/blog/2020-12-16-aligning-ror-with-posi/) (original posted 2020-December-16)
21. [Sciety](https://blog.sciety.org/open-scholarly-infrastructure/) (original posted 2021-November-22)
22. [SciPost](https://scipost.org/posi) (original posted 2024-March-11)

> If you are considering adopting POSI and would like to chat about it, please reach out to any colleagues at any of the organizations above.

---
