---
title: Proposed POSI 2.0 revisions
subtitle: A request for public comment on proposed revisions to POSI.
date: "2025-01-21"
publishdate: "2025-01-21"
comments: false
---

## We Want Your Feedback: Help Shape the Future of POSI

We’re excited to announce that we are gathering feedback on the proposed revisions to the [Principles of Open Scholarly Infrastructure (POSI)](https://openscholarlyinfrastructure.org), drafted by the existing ~20 adopters and based on real-world experience of following them.

The [original blog post about POSI](http://dx.doi.org/10.6084/m9.figshare.1314859) (considered the 1.0 release) was in 2015 and, since 2020, 22 organizations and initiatives have adopted the principles and done self assessments. In November 2023, the POSI Adopters released [version 1.1 of the principles](https://doi.org/10.24343/C34W2H). The revisions were clarifications based on the experience of the adopters at that time.  

[The POSI adopters](/posse/) are now undertaking a public consultation on proposed revisions for a version  2.0 release of the principles. This is a crucial step in ensuring that POSI evolves to meet the needs of the community. Whether you are part of an organization that has adopted POSI, is considering adoption, or operates outside its scope but interacts with POSI-aligned organizations, your perspective is crucial.

Some additional context about POSI:

* POSI is not an organization; POSI adopters are an informal group of those that have conducted self-assessments.

* The POSI principles are not rules or a checklist; organizations or groups can adopt or interpret them to fit many different circumstances.

* Our goal is for POSI self-assessments to be made publicly available and for interested communities to assess and monitor updates and progress.

### How to Participate

We invite you to [review the proposed changes](/proposed-posi-v2-revisions.pdf) and share your thoughts through a [short survey](https://forms.gle/4KkaRJoar6KjrsbW7). Your feedback will be instrumental in refining the revisions to make them practical, effective, and relevant.

### Submit Your Feedback

The feedback process is open now and will close on March 5th, 2025. We encourage you to participate and help shape the future of open scholarly infrastructure.

* A summary of the proposed changes is available. Please review the summary of the proposed changes ["Proposed POSI 2.0 revisions" PDF, 630Kb](/proposed-posi-v2-revisions.pdf)

* Please then [take the survey](https://forms.gle/4KkaRJoar6KjrsbW7). The deadline is March 5, 2025.

Thank you for contributing to the advancement of open scholarly infrastructure!